<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville|Roboto|Roboto+Condensed" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
</head>


<body class="HolyGrail">

<header headroom>
	<div class="headerL">
		<div id="ockerL" class="colLeftChild">&nbsp;  </div>
		<div id="braunL" class="colLeftChild">&nbsp;  </div>
		<div id="schwarzL" class="colLeftChild">&nbsp; </div>
	</div>
	<div class="headerNav">
		<div id="navigation">	
		
	<?php if ( frontier_option('main_menu_enable', 1) == 1 ) : ?>
		<?php $menu_style = frontier_option('main_menu_style', 'stack'); ?>

		<nav id="nav-main" class="cf <?php echo $menu_style; ?>" <?php frontier_schema( 'nav-main' ); ?>>
			<?php do_action('frontier_before_menu'); ?>

			<?php if ( $menu_style == 'drop' ) : ?>
				<a href="#" class="drop-toggle"><span class="genericon genericon-menu"></span></a>
			<?php endif; ?>

			<?php wp_nav_menu( array(
				'theme_location' 	=> 'frontier-menu-primary',
				'container' 		=> false,
				'menu_class' 		=> 'nav-main',
				'fallback_cb'		=> 'wp_page_menu' ) );
			?>

			<?php do_action('frontier_after_menu'); ?>
		</nav>
	<?php endif; ?>
	
			<div class="headerStreifenOcker"></div>
			<div class="headerStreifenSchwarz"></div>
		</div>
	
	</div>
	<div class="headerL">
		<div id="ockerR" class="colRightChild">&nbsp; </div>
		<div id="braunR" class="colRightChild">&nbsp;  </div>
		<div id="schwarzR" class="colRightChild">&nbsp;  </div>
	</div>
</header>


<?php do_action('frontier_before_main'); ?>