<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(calendar); ?>

<div class="HolyGrail-body">

    <!-- Content -->
	<main class="HolyGrail-content">

<div id="content" class="cf" <?php frontier_schema( 'content' ); ?>>
<?php do_action('frontier_before_content'); ?>

<?php do_action('frontier_before_loop'); ?>

<?php if (have_posts()): ?>
	<article id="post-<?php the_ID(); ?>" class="page type-page status-publish hentry post single-event">
		<section class="main">

			<?php the_post(); ?>
		
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		
		</section>
	</article>

<?php endif; ?>

<?php do_action('frontier_after_content'); ?>
</div>
	
	</main>
	
	<!-- linke Spalte -->
	<nav class="HolyGrail-nav">
		<div id="logoBar">
			<div class="logoFix"></div>
			<div class="logoBack_t"></div>
			<div class="logoBack_b"></div>
			<div class="logoBlau">
				<div class="logoStreifenUnten"></div>
			</div>
		</div>
	</nav>
	
	<!-- rechte Spalte -->
    <aside class="HolyGrail-ads">
		<div class="kaempfer"></div>
	</aside>
	
  </div>

<?php get_footer(); ?>