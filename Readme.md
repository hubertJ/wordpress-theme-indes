# Wordpress Theme - INDES

Das Theme ist ein Child Theme von Frontier und umfasst Änderungen an Wordpress, WooCommerce, All in One Event Calender und NextGallery. Die Anpassungen der letzten zwei Plugins sind in eigenen Child Themes umgesetzt und nicht in diesem Paket enthalten.

Verwendete Umgebung:
* node.js
* npm
* bower
* gulp

Eingebundene Pakete:
* headroom.js

## Vorlage
![alt text](https://gitlab.com/hubertJ/wordpress-theme-indes/raw/326a01da0d9cd8b82e24713f98b393fca24e2215/Design9a.jpg "Vorlage")

## Installation

* node.js installieren: https://nodejs.org/en/
* npm global installieren: `$ npm install npm -g`
* bower global installieren: `$ npm install -g bower`

Im Projektordner:
* gulp installieren: `$ npm install --save-dev gulp-install`
* Alle Paket-Abhängigkeiten installieren: `$ npm install`
* headroom.js via bower installieren: `$ bower install headroom.js`


## Einrichtung

In der gulpfile.js evtl. Optionen für den Autoprefixer ändern, Standard:

    .pipe(plugins.autoprefixer({
	    browsers: ['>0.21% in at'],
	    cascade: true
    }))

Symlink in Wordpress Installation zu /build/ erstellen (ln / mklink). Achtung bei mklink: der Link muss als Ordner ausgeführt sein ( /D ). Die zu bearbeitenden Dateien befinden sich alle in /src/. 

## Ausführung

* Ausführen von gulp: `$ gulp`
* Erstellen und Aktualisieren der Distribution: `$ gulp dist`