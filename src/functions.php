<?php
function theme_enqueue_scripts() {
    wp_register_script('custom-js',get_stylesheet_directory_uri().'/js/scripts.js',array(),NULL,true);
    wp_enqueue_script('custom-js');

    $wnm_custom = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
    wp_localize_script( 'custom-js', 'directory_uri', $wnm_custom );
}

if (! is_admin() ) {
    // Einfügen der scripts.js und Theme URL in JS Variable schreiben
    add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
}

function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/rtl.css' );
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/editor-style.css' );
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/responsive.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/responsive.css', array('parent_style') );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
load_theme_textdomain( 'frontier', get_stylesheet_directory() . '/languages' );

add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'customize' );
	$wp_admin_bar->remove_node( 'frontier_admin_bar' );
	$wp_admin_bar->remove_node( 'w3tc' );
}

// Hilfebox im Dashboard
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Hilfe', 'custom_dashboard_help');
}

function custom_dashboard_help() {
    // $url = get_stylesheet_directory_uri();
	echo '<p><a href="https://wiki.indes.ninja/de/Fachbereiche/Technik/AnleitungWebseite" target="_blank">Hilfe für Anwender</a></p><p><b>Achtung bei Updates für All in One Event Calendar und Frontier Theme</b>, nicht durchführen, Hubert machen lassen!</p>';
}

// WooCommerce Anpassungen
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('woocommerce/woocommerce.php')) {
    add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

    // Our hooked in function - $address_fields is passed via the filter!
    function custom_override_default_address_fields( $address_fields ) {
        $address_fields['company']['label'] = "INDES Standort";
        $address_fields['company']['required'] = true;
        $address_fields['company']['type'] = 'select';
        $address_fields['company']['options'] = array(
            'INDES Salzburg' => __('INDES Salzburg', 'woocommerce' ),
            'INDES Graz' => __('INDES Graz', 'woocommerce' ),
            'INDES Klagenfurt' => __('INDES Klagenfurt', 'woocommerce' ),
            'INDES Kulmbach' => __('INDES Kulmbach', 'woocommerce' ),
            'INDES Wien' => __('INDES NÖ/Wien', 'woocommerce' ),
            'INDES Linz' => __('INDES Linz', 'woocommerce' )
            );

        return $address_fields;
    }

    add_filter( 'wc_product_sku_enabled', '__return_false' );

    // Display 24 products per page. Goes in functions.php
    add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 30;' ), 20 );

    add_action( 'after_setup_theme', 'woocommerce_support' );
    function woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }

    // Place the following code in your theme's functions.php file to add the payment type to all emails
    add_action( 'woocommerce_email_before_order_table', 'wc_add_reason_for_payment', 15, 2 );
    function wc_add_reason_for_payment( $order, $is_admin_email ) {
        $id = $order->get_order_number();
        echo 'Verwendungszweck: Ausrüstungsbestellung-' . $id;
    }

}

/**
 * Check whether we are on a subpage
 *
 * @return mixed ID of the parent post or false if this is not a subpage.
 */
function wpdocs_is_subpage() {
    // Load details about this page.
    $post = get_post();
 
    // test to see if the page has a parent
    if ( is_page() && $post->post_parent ) {
        // Return the ID of the parent post.
        return $post->post_parent;
    // There is no parent so ...
    } else {
        // ... The answer to the question is false
            return false;
    }
}

//	Eingeschränktes Menü für Spezialseiten des Shops (Mein Konto, Kasse, Warenkorb)
function custom_menu () {
	// ggf. anpassen
	if( is_page( array ('kasse', 'mein-konto', 'warenkorb'))) {
		wp_nav_menu( array(
			'menu'				=> 'shopmenu',
			'container' 		=> false,
			'menu_class' 		=> 'nav-main',
			'fallback_cb'		=> 'wp_page_menu' ));
	}
	else {
		wp_nav_menu( array(
			'theme_location' 	=> 'frontier-menu-primary',
			'container' 		=> false,
			'menu_class' 		=> 'nav-main',
			'fallback_cb'		=> 'wp_page_menu' ));	
	}				
}


// Progress Bar for akademie.indes.at
function progress_bar ( $attr ) {
    $maxwidth = "80";
    $actualwidth = 100 * ( $attr["actual"] / $attr["goal"] );
    $content = '<div class="progress_bar_wrapper">';
    $content .= '<div class="progress_bar">'. $attr["actual"] .'€ / '. $attr["goal"] .'€</div>';
    $content .= '<div class="progress_graphic">';
    $content .= '<div style="width:'. $actualwidth .'%"></div>';
    $content .= '</div></div>';
    return $content;
}
add_shortcode('progress', 'progress_bar');


// Spoiler Funktion
function spoiler_box ( $attr, $content = null ) {
    $c = '<input type="checkbox" id="spoiler'. $attr["nr"] .'" class="spoiler_input" /><label for="spoiler'. $attr["nr"] .'">'. $attr["label"] .'</label>';
    $c .= '<div class="spoiler">'. $content . '</div>';
    return $c;
}
add_shortcode('spoiler', 'spoiler_box');


// event submits progress bar shortcode
function event_progress_bar_shortcode( $attr ) {
    $form_id = intval( $attr['id'] );
    $max = intval( $attr['max'] );

    $form = Ninja_Forms()->form( $form_id )->get_subs();
    $subs = count( $form );
    $width = ( $subs / $max ) * 100;

    $date_now = new DateTime( $attr['end_date']);
    $date_end = new DateTime();
    $diff = $date_end->diff( $date_now )->format( "%a" );
    $days = intval( $diff );

    $html = '
        <p style="text-align:center">
            <p style="text-align:center"><strong>Aktueller Anmeldestand:</strong> ('. $days .' Tage übrig)</p>
            <div class="event-progressbar__container">
                '. $subs .' / '. $max .'
                <div style="width:'. $width .'%" class="event-progressbar__bar"></div>
            </div>
        </p>
    ';
    return $html;
}
add_shortcode( 'event_progress_bar', 'event_progress_bar_shortcode' );


// course submissions progress shortcode
function course_submissions_list_shortcode( $attr ) {
    $html = '';
    $form_id = intval( $attr['id'] );
    $option_key = $attr['option_key'];
    $option_slug = $attr['option_slug'];
    $option_max = intval( $attr['option_max'] );
    $sub_max = intval( $attr['sub_max'] );
    $option = array();
        
    $subs = Ninja_Forms()->form( $form_id )->get_subs();

    foreach( $subs as $sub ) {

        $form   = Ninja_Forms()->form( $form_id );
        $fields = $form->get_fields();

        $fieldKeys = [];
        foreach ( $fields as $field_id => $field ) {
            $fieldKeys[ $field->get_setting( 'key' ) ] = $field->get_id();
        }
        ksort( $fieldKeys );

        $field = $sub->get_field_value( $fieldKeys[ $option_slug ] );

        // $field = $sub->get_field_value( $option_slug );

        for( $i=1; $i<=$option_max; $i++ ) {
            if( $field == $i ) {
                $option[$field]++;
            }
        }
    }
    ksort( $option );

    $html .= '<ul>';
    for( $i=1; $i<=$option_max; $i++ ) {
        if( $option ) {
            $amount = ( $option[$i] ) ? $option[$i] : '0';
            $max_reached = ( $option[$i] >= $sub_max ) ? 'subs_max_reached' : '';
        } else {
            $amount = '0';
        }
        $width = ( $amount / $sub_max ) * 100;
        if( $width > 100 ) {
            $width = 100;
        }
        $html .=    '<li class="course-submission '. $max_reached .'">
                        <div class="course-submission__data">
                            <div class="course-submission__data__desc">'. $option_key . ' ' . $i . ': </div>
                            <div class="course-submission__data__numbers">'. $amount .' / '. $sub_max .'</div>
                        </div>
                        <div class="course-submission__bar">
                            <div style="width:'. $width .'%" class="course-submission__bar__inner"></div>
                        </div>
                    </li>';
    }
    $html .= '</ul>';

    return $html;
}
add_shortcode( 'course_submissions_list', 'course_submissions_list_shortcode' );


function course_submission_shortcode( $attr ) {
    $html = '';
    $form_id = intval( $attr['id'] );
    $option_slug = $attr['option_slug'];
    $option_nr = intval( $attr['option_nr'] );
    $sub_max = intval( $attr['sub_max'] );
    $description = $attr['description'];
    $amount = 0;

    $subs = Ninja_Forms()->form( $form_id )->get_subs();
    foreach( $subs as $sub ) {
        $form   = Ninja_Forms()->form( $form_id );
        $fields = $form->get_fields();

        $fieldKeys = [];
        foreach ( $fields as $field_id => $field ) {
            $fieldKeys[ $field->get_setting( 'key' ) ] = $field->get_id();
        }
        ksort( $fieldKeys );

        $field = $sub->get_field_value( $fieldKeys[ $option_slug ] );
        if( $field == $option_nr ) {
            $amount++;
        }
    }

    $max_reached = ( $amount >= $sub_max ) ? 'subs_max_reached' : '';
    $width = ( $amount / $sub_max ) * 100;
    if( $width > 100 ) {
        $width = 100;
    }
    $html .=    '<div class="course-submission '. $max_reached .' course-submission-single" data-id="'. $option_nr .'">
                    <div class="course-submission__data">
                    <div class="course-submission__data__desc">'. $description .'</div>
                        <div>'. $amount .' / '. $sub_max .'</div>
                    </div>
                    <div class="course-submission__bar">
                        <div style="width:'. $width .'%" class="course-submission__bar__inner"></div>
                    </div>
                </div>';

    return $html;
}
add_shortcode( 'course_submission', 'course_submission_shortcode' );


function set_addressbar_color() {
	$color = '#0F2882';
	echo '<meta name="theme-color" content="'.$color.'">';
}
add_action( 'wp_head', 'set_addressbar_color' );


function var_error_log( $object=null ){
    ob_start();                    // start buffer capture
    var_dump( $object );           // dump the values
    $contents = ob_get_contents(); // put the buffer into a variable
    ob_end_clean();                // end capture
    error_log( $contents );        // log contents of the result of var_dump( $object )
}