# Anleitung für www.indes.at

In dieser Anleitung soll es nicht um die grundlegende Funktionalität von Wordpress gehen, sondern vielmehr um die zahlreichen Anpassungen, die vorgenommen wurden um die Software an unsere Bedürfnisse anzupassen. Eine Einführung in das Thema Wordpress findest du hier:
 
* [wordpress_einführung.pdf](http://indes.at/indes/anleitung/wordpress_einfuehrung.pdf)
* http://www.reetdachhaus-puddemin.de/wp-content/uploads/2014/01/wp-38-autoren-redakteure.pdf
 
Eines der wichtigsten Plugins für unsere Seite ist der PageBuilder. Damit ändert sich die Herangehensweise an die Erstellung neuer Inhalte. Viele bereits vorhandene Seiten und Beiträge sind damit aufgebaut, grundlegende Dokumentation zum Pagebuilder findet sich hier:
 
* https://siteorigin.com/page-builder/documentation/  (die Anleitung des Herstellers, englisch, mit Video)
* https://lichtweb.ch/wordpress-siteorigin-pagebuilder/  (Videos mit Schweizer Akzent…)

---

# Inhaltsverzeichnis
- [Anleitung für www.indes.at](#anleitung-f%c3%bcr-wwwindesat)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Inhalte erstellen](#inhalte-erstellen)
  - [Seiten und Beiträge](#seiten-und-beitr%c3%a4ge)
    - [Titel/Untertitel und blauen Streifen](#titeluntertitel-und-blauen-streifen)
    - [Bilder im Header](#bilder-im-header)
    - [Tabellen](#tabellen)
    - [Standorticon](#standorticon)
    - [Alle Widget-Klassen](#alle-widget-klassen)
  - [Baumstruktur](#baumstruktur)
  - [Veranstaltungen](#veranstaltungen)
  - [Weitere Hinweise und Tipps](#weitere-hinweise-und-tipps)
    
---

## Inhalte erstellen
 
Es gibt mehrere Möglichkeiten, Inhalte zu erstellen, die wichtigsten Arten sind:
 
* Seiten bilden das Grundgerüst der Seite und beinhalten Informationen, die sich selten ändern. Dazu gehören die Startseite, die Menüpunkte Trainingsorte, Fechtlehrer usw.
* Beiträge stellen die Inhalte des Federspiels dar.
* Veranstaltungen bilden die Inhalte des Kalenders. Weist man einer Veranstaltung die Kategorie „Seminare“ zu, wird sie auch in der Ansicht „Seminare“ aufscheinen.
 
Zur Erstellung von Seiten und Beiträgen können dieselben Gestaltungsmöglichkeiten genutzt werden, hier kommt das Plugin PageBuilder zum Einsatz. Bei Veranstaltungen gilt dies hingegen nicht, hier können nur die Möglichkeiten des Standardeditors zum Einsatz gebracht werden.

---

## Seiten und Beiträge
 
### Titel/Untertitel und blauen Streifen
 
Die Seite „HEMA“ dient hier als Beispiel, um die Elemente der Seite identifizieren zu können. Gegenübergestellt die Ansicht im Editor.

![Bild](images/image001.jpg)

Die Seite besteht aus zwei Blöcken:
 
1. **normaler Textblock** – dieser Block hat die Hintergrundfarbe des Inhaltsbereiches der Seite. Das Besondere hier ist die Nutzung von Titel und Untertitel. Dazu später mehr.
 
2. **Textblock in blauem Streifen** – der zweite Block nutzt hingegen die alternative Hintergrundfarbe blau, mit einer Einfassung aus einem weißen Streifen oben und einem schwarzen Streifen mit Schlagschatten unten. Hier wurde kein Untertitel verwendet.
 
Im Editor (immer zu erreichen über „Seite bearbeiten“ in der obersten, schwarzen Leiste. Sieht man diese nicht, ist man nicht eingeloggt) sieht die Seite wie folgt aus:

![Bild](images/image002.jpg)

Der erste Block umfasst drei Widgets innerhalb einer Zeile, allesamt „SiteOrigin Editor“ Widgets. Die sehen im Detail so aus:

![Bild](images/image003.jpg)

**Titel**: Das Widget für den Titel beinhaltet nur den Titeltext, eingetragen in das Formularfeld „Title“.
Wichtig: damit alle Elemente am Ende wie gewünscht aussehen, muss einigen von ihnen eine Widget-Klasse zugewiesen werden. Dies geschieht über die rechte Spalte des Editors, dort unter Attribute -> Widget-Klasse. Für einen Titel wird hier der Wert „text_titel“ eingetragen.
 
**Untertitel**: Das Widget für den Untertitel enthält ausschließlich den Text für den Untertitel, dieser wird in das normale Textfeld eingetragen. Als Widget-Klasse wird „text_untertitel“ eingetragen.
 
**Fließtext**: Der Fließtext wird ohne Angabe eines Titels in das dritte Widget eingegeben. Widget-Klassen sind hier nicht anzugeben.
 
 
 
Der zweite Block enthält ein einzelnes Widget in einer Zeile. In diesem Widget wird der Titel des Blocks (hier: „LINKS“) in das Formularfeld „Title“ und der Fließtext in das Textfeld eingetragen. Dem Widget selbst wird keine Widget-Klasse zugewiesen. Der blaue Streifen wird über die Eigenschaften der Zeile generiert, zu denen gelangt man wie folgt:

![Bild](images/image004.jpg)

Im Formular „Zeile bearbeiten“ sucht man nun unter Attribute das Formularfeld „Zeilen-Klasse“ und gibt dort „text_blauerstreifen“ ein:

![Bild](images/image005.jpg)

### Bilder im Header
 
Ein weiteres häufig benutztes Designelement ist das große Bild im Header, welches nahtlos an die Talhoffer Grafik in der rechten Spalte anschließt. Dies kann ein einzelnes Bild oder auch eine Sammlung von Bildern sein, die als Slider ausgeführt werden.
Um ein einzelnes Bild einzufügen, wird eine Zeile im PageBuilder hinzugefügt, die unbedingt an oberster Stelle gereiht werden muss. In diese Zeile wird das Widget „SiteOrigin Image“ eingefügt, in welchem ein geeignetes Bild ausgewählt wird.
 
Achtung:
* **Damit der nahtlose Anschluss gelingt, muss das Bild die Maße 960px * 361px aufweisen.**
* **Die Option „Full Width“ im Widget muss angewählt werden.**
 
Für eine Bilderserie als Slider werden ebenfalls Bilder in den Maßen 960px * 361px benötigt, eingebunden werden sie mittels dem Widget „SiteOrigin Slider“.


### Tabellen
 
Für Tabellen kann die Tabellenfunktion des Editors benutzt werden, allerdings sprengen Tabellen oft das Design in der mobilen Ansicht. Um dem entgegen zu wirken, kann man die Tabellen mit einem sogenannten div Container umgeben, welcher seinem Inhalt bei Überbreite eine horizontale Scrollbar gibt. Damit wird nicht mehr die gesamte Seite zur Seite scrollbar, sondern nur mehr der Bereich in dem sich die Tabelle befindet.
Um das umzusetzen, muss in den „Text“ Reiter des Editors gewechselt werden (Standard ist „Visuell“). Dort sucht man den Anfang der Tabelle (-> `<table>` ) und fügt folgendes vorher ein:
 
`<div class="text_table">`
 
Dann sucht man das Ende der Tabelle (-> `</table>` ) und fügt dahinter ein:
 
`</div>`

### Standorticon

Um ein Standorticon einzufügen, kann man die untenstehenden Widget Klassen nutzen, dies funktioniert aber in der mobilen Ansicht eher schlecht. Besser ist folgende Lösung:

Eine neue Zeile erstellen mit der Row-ID `#text_titelmiticon`, darin ein Widget "Text" einfügen. Beim Bearbeiten des Widgets auf die Ansicht Text wechseln und folgendes HTML eingeben, dabei müssen die Platzhalten TITEL, UNTERTITEL, URL ZUM STANDORT und URL ZUM STANDORTICON ausgetauscht werden:

```html
<div id="css_titelmiticon">
    <div class="css_titelblock">
        <div class="css_titel widget-title">
            <h3> TITEL </h3>
        </div>
        <div class="css_untertitel"> UNTERTITEL </div>
    </div>
    <div class="css_standorticon"><a href=" URL ZUM STANDORT "><img src=" URL ZUM STANDORTICON " /></a></div>
</div>
```
Achtung: In der Voransicht des Editors sieht das Ergebnis falsch aus! Nicht davon beirren lassen.

### Alle Widget-Klassen
 
Liste aller Widget-Klassen, die verwendet werden können:
 
* **text_titel**, sorgt für die Ausrichtung von Titeln in Zusammenspiel mit text_untertitel, wirkt sich auf die Title Eingabe eines Editor-Widgets aus.
* **text_untertitel**, erstellt Untertitel mit schwarzem Streifen als Hintergrund, wirkt sich auf die Texteingabe eines Editor-Widgets aus.
* **text_blauerstreifen**, gibt der Zeile einen blauen Hintergrund mit weißem Streifen oben und schwarzem Streifen mit Schlagschatten unten, wirkt sich auf die Zeilen-Klasse einer Zeile aus.
* **text_header**, erstellt ein Headerbild oder Header-Bilderslider wie auf der Startseite, wirkt sich auf ein „SiteOrigin Image“ oder „SiteOrigin Slider“ Widget aus.
* **text_table**, Klasse für Tabellen, damit diese in der mobilen Ansicht horizontal scrollbar sind.
* **text_zweispaltig**, erstellt zweispaltigen Text mit vertikaler Trennlinie, wirkt sich auf die Texteingabe eines Editor-Widgets aus.
* **veraltet (bessere Möglichkeit siehe oben)**: Will man ein Standorticon in eine Seite einbauen bedarf es folgender Klassen:
	* **text_titelmiticon**, Anpassung von text_titel
	* **text_standorticon**, Klasse für das Standorticon.
* **text_produktkommentar**, wird im Ausrüstungsleitfaden für Kommentare einzelner Produkte genutzt, die beim Laden der Seite nicht komplett angezeigt und erst aufgeklappt werden müssen.
* **text_leitfaden**, wenn diese Klasse in einer Zeile angegeben wird, erhält die Zeile einen dünnen Rahmen an ihrer Unterseite. Genutzt wird dies primär im Ausrüstungsleitfaden.
 
---

## Baumstruktur
 
Seiten können (und sollen!) in einer Baumstruktur angeordnet werden. Man könnte dies realisieren, indem man bei jeder Seite das korrekte Elternelement angibt (das wird über das Formular Seiten-Attribute in der rechten Spalte der Editoransicht gemacht), viel einfacher ist aber der Weg über den Menüpunkt Seiten -> Baumansicht. Sieht wie folgt aus:

![Bild](images/image006.jpg)

Exemplarisch ist hier die Verästelung für den Standort Graz ausgeklappt. Von hier aus kann jede Seite via Drag and Drop an den gewünschten Platz verschoben werden. Außerdem können für jede Seite die Links zur Bearbeitung und Ansicht eingeblendet, sowie neue Seiten erstellt werden, direkt an der richtigen Stelle der Struktur.
Ein großer Vorteil der Baumstruktur ist übrigens das Generieren von sehr übersichtlichen und leicht lesbaren Links. Die im Bild hervorgehobene Seite „Langes Schwert Grundlagen“ im Trainingsangebot von Graz ist z.B. über diesen Link erreichbar: http://www.indes.at/trainingsort/graz/angebot/langes-schwert-grundlagen/
 
--- 
 
## Veranstaltungen
 
Veranstaltungen können sowohl **Kategorien** als auch **Schlagwörter** zugewiesen werden. Schlagwörter können beliebig neu erstellt werden über das entsprechende Formular in der rechten Spalte der Editor Ansicht. Als Kategorien finden sich übergeordnete Themen wie Standorte, Waffen/Trainingsangebote und Seminare. Die große Stärke des Kalenders ist die Möglichkeit, ihn immer wieder auf Seiten oder Beiträgen einzubinden, und das auch noch mit vorgegebener Filterung.
Man kann also den Kalender z.B. so einbinden: http://www.indes.at/trainingsort/graz/#termine
Der Link zeigt die Seite des Standortes Graz (mit einem Sprung auf den Anker „Termine“), in dem der Kalender dargestellt wird mit allen Veranstaltungen, denen die Kategorie „INDES Graz“ zugewiesen wurde. Das wird umgesetzt mittels eines sogenannten Shortcodes. Man fügt an die gewünschte Stelle folgendes ein:
 
`[ai1ec cat_name="Kategorie"]`

Im Beispiel von Graz also: `[ai1ec cat_name="INDES Graz"]`
 
Will man mehrere Kategorien zeigen, schreibt man sie hintereinander hin und trennt sie mit einem Beistrich. Also, z.B.: `[ai1ec cat_name="INDES Graz,Seminare"]`  So werden alle Veranstaltungen mit den Kategorien „INDES Graz“ oder „Seminare“ angezeigt.
 
Die Seite „Seminare“ funktioniert mit demselben Prinzip, Unterschied hier ist die Art der Darstellung. Wurde im vorigen Beispiel noch die Ansicht „Monat“ gewählt, werden die Seminare via der Ansicht „Liste“ eingebunden. Der entsprechende Code lautet:
 
`[ai1ec view=“agenda“ cat_name="Kategorie"]`
 
Im Beispiel „Seminare“ also: `[ai1ec view=“agenda“ cat_name="Seminare"]`
 
**Achtung**: Mehrere Kategorien werden als ODER Verknüpfung ausgeführt, es werden also alle Veranstaltungen gezeigt, denen entweder die erste angegebene Kategorie oder eine der weiteren angegeben Kategorien zugewiesen wurden. Für eine detailliertere Ansicht des Kalenders benötigen wir die Schlagwörter.
 
 
Mit Schlagwörtern können also detailliertere Zuweisungen gemacht werden, z.B. ein konkretes Trainingsangebot eines Standortes. Beispiel: „Graz_Stangenwaffen“ ist als Schlagwort jedem Trainingsangebot des Standortes Graz zugewiesen worden, welches Stangenwaffen beinhaltet. Damit kann ein Kalender in der Seite des entsprechenden Trainingsangebotes dargestellt werden:  http://www.indes.at/trainingsort/graz/angebot/stangenwaffen/
 
Der notwendige Code dafür lautet:
 
`[ai1ec tag_name="Schlagwort"]`
 
Im Beispiel der Stangenwaffeneinheit in Graz also: `[ai1ec tag_name="Graz_Stangenwaffen"]`
 
---

## Weitere Hinweise und Tipps
 
* Sangar Slider bitte nicht benutzen, der ist ausschließlich für die Startseite gedacht.
* Bei Einfügen von Galerien in Text ist darauf zu achten, dass bei einer Wahl der Ansicht „Basic Thumbnails“ die Option „Add Automatic Paragraphs“ im Editor unbedingt deaktiviert wird. Geschieht das nicht, wird zwischen Bild und Rahmen ein Zeilenumbruch hinzugefügt.