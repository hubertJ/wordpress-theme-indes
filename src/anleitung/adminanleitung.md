# Anleitung für Admins von www.indes.at

## Updates

Folgende Dateien von Themes und Plugins wurden derart verändert, dass sie bei einem Update kontrolliert werden müssen:

* All in One Event Calendar -> `/wp-content/plugins/all-in-one-event-calendar/app/view/event/avatar.php`
Die Funktion `get_event_avatar_url`  wurde durch den Inhalt von `get_featured_image_url` ersetzt. Der Hack sorgt dafür, dass stets das größtmögliche Bild der Veranstaltung geladen wird, statt der 300px breiten Verkleinerung. Wichtig für die Seminaransicht, funktionieren dort die Bilder nicht richtig (unscharf, zu klein), hat das Update die Funktion wieder überschrieben (was aufgrund der Methodik von Updates bei Wordpress auf jeden Fall passieren wird).
* All in One Event Calendar -> `/wp-content/plugins/all-in-one-event-calendar/lib/import-export/ics.php`
```php
$content = apply_filters(
	'ai1ec_the_content',
	apply_filters(
		'the_content',
		$event->get( 'post' )->post_content
	)
);
```        
Der Filter `the_content` wird vom PageBuilder Plugin verändert und führt zur Inkompatibilität mit dem Kalenderexport. Workaround, Filter weglassen: `$content = $event->get( 'post' )->post_content;`.

---

## CSS

* All In One Event Calendar -> `/wp-content/themes-ai1ec/indes/css/override.css`
Das CSS wird erst übernommen, wenn im Adminpanel unter Veranstaltungen –> Theme Einstellungen -> Einstellungen speichern angeklickt wird. Achtung: nicht das Formular zum Anpassen von Farben etc. verwenden, diese Einstellungen gehen verloren wenn das Theme gewechselt wird.
Eine Einstellung ist aber notwendig, da nicht über die override.css erfasst: Monats-/Wochen-/Tagesansicht -> Hintergrund der Veranstaltung: #e1c67b.
* Sangar Slider -> Das CSS ist über den Slider selbst zu finden, im Adminpanel den Slider editieren, dann Custom CSS.
* NextGen Gallery -> `/wp-content/ngg_styles/nggallery.css`

---

## GitLab

* Haupt-Theme mit Entwicklungsumgebung: https://gitlab.com/hubertJ/wordpress-theme-indes (öffentlich)
* Haupt-Theme, zum Verteilen: https://gitlab.com/hubertJ/indes-theme
* All in One Event Calender Child Theme: https://gitlab.com/hubertJ/wordpress-theme-indes-ai1ec