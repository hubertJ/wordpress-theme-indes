var theme_url = directory_uri.stylesheet_directory_uri;

// Die Animation von .nav-main soll bei Seitenaufruf nicht sichtbar ablaufen, dafür wird in die ID #nav-main die Klasse
// loadComplete bei abgeschlossenem Laden der Seite eingefügt und anschließend 600ms gewartet (die Animation dauert 400ms)
jQuery( function() {
	var delayMillis = 600; 
	setTimeout(function() {
		document.getElementById("nav-main").classList.add("loadComplete");
	}, delayMillis);
});

// Wenn die Klasse .dropped existiert (-> das Menü gezeigt wird), soll der Link zum Einklappen des Menüs getriggert werden
jQuery(".nav-main, .HolyGrail-content").on("click",function() {
	if ( jQuery( "#nav-main" ).hasClass( "dropped") ) {
		jQuery( "#nav-main > .drop-toggle" ).trigger( "click" );
	}
});

// Spoilerkasten für die Kommentare im Leitfaden
jQuery( "[class^=togglespoiler]" ).click(function() {
	var str = this.className;
	var showclass = str.replace("toggle","");
	jQuery( "[class=" + showclass + "]" ).toggle( "100", function() {
		var text = jQuery("[class=" + str + "]").html();
		if (text >= "Kommentare einblenden") {
			jQuery( "[class=" + str + "]").html("Kommentare ausblenden <img src=" + theme_url + "/images/leitfaden_up.png>");
		} else {
			jQuery( "[class=" + str + "]").html("Kommentare einblenden <img src=" + theme_url + "/images/leitfaden_down.png>");
		}
	});
});



jQuery(document).ready(function($) {
	
	if( $( '.blog__slider-and-text' ).length > 0 ) {
		$( '.panel-layout' ).addClass( 'blog--no-margins' );
	}


	$( '#menu-haupt li.menu-item-has-children' ).each( function() {
		var chevronDown = '<a class="open-sub"></a>';
		var menuLink = $( this ).children( 'a' );
		menuLink.wrap( '<div class="open-sub__wrapper"></div>' );
		$( this ).find( '.open-sub__wrapper' ).append( chevronDown );
	});

	$( '#menu-haupt li.menu-item-has-children .open-sub' ).on( 'click', function( e ) {
		e.preventDefault();
		$( this ).toggleClass( 'open-sub--opened' );
		$( this ).parents( 'li' ).find( '.sub-menu' ).toggleClass( 'sub-menu--show' );
		return false;
	})

	setTimeout( function() {
		$( '.course-submission' ).each( function() {
			var groupNr = $( this ).attr( 'data-id' );

			if( $( this ).hasClass( 'subs_max_reached' ) ) {
				console.log( $( this ) );
				console.log( $( this ).classList );
				console.log( 'subs_max_reached found' );
				$( '.ninja-forms-field[type="radio"][value="' + groupNr + '"]' ).parents( 'li' ).addClass( 'course_max_reached' );
			}
		})
	}, 1000 );

});