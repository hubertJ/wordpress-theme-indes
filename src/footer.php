<footer class="HolyGrail-footer">
	<div class="footerL">&nbsp;</div>
	<div id="bottom-bar" class="cf">
	<?php do_action('frontier_before_bottom_bar'); ?>

	<?php if ( frontier_option('bottom_bar_text', get_bloginfo('name') . ' &copy; ' . date('Y')) ) : ?>
		<span id="bottom-bar-text"><?php echo frontier_option('bottom_bar_text', get_bloginfo('name') . ' &copy; ' . date('Y')); ?></span>
	<?php endif; ?>

	<?php if ( frontier_option('theme_link_disable', 0) == 0 ) : ?>
		<?php $frontier_theme_link = '<a href="' . esc_url( 'http://ronangelo.com/frontier/' ) . '">Frontier Theme</a>'; ?>
		<span id="theme-link"><?php echo apply_filters( 'frontier_theme_link', $frontier_theme_link ); ?></span>
	<?php endif; ?>
	
	<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
		if (is_plugin_active('social-media-buttons-toolbar/social-media-buttons-toolbar.php')) {
			echo smbtoolbar_shortcode();
		}
	?>
	<?php do_action('frontier_after_bottom_bar'); ?>
	</div>

<?php do_action('frontier_after_container'); ?>
		
		
        </div>
		
</footer>
 
<?php do_action('frontier_after_body'); ?>
 
<?php wp_footer(); ?>


<script type="text/javascript">
	var myElement = document.querySelector("header");
	var headroom = new Headroom(myElement, {
	  "offset": 0,
	  "tolerance": 1,
	  "classes": {
	    "initial": "animated",
	    "pinned": "slideDown",
	    "unpinned": "slideUp"
	  }
	});
	headroom.init();
</script>

</body>
</html>