var gulp          = require('gulp'),
    browserSync   = require('browser-sync').create(),
    fileinclude   = require('gulp-file-include'),
    rename        = require('gulp-rename'),
    plugins       = require("gulp-load-plugins")({
                        pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
                        replaceString: /\bgulp[\-.]/
                    });    

gulp.task( 'bowerFilesJS', function() { 
    return gulp.src( plugins.mainBowerFiles('**/*.min.js'))
        .pipe(gulp.dest('src/js'));
});

gulp.task( 'bowerFilesCSS', function() {
    return gulp.src( plugins.mainBowerFiles('**/*.scss'))
        .pipe(plugins.rename({
            prefix: "_"
        }))
        .pipe(gulp.dest('src/scss'));
});

gulp.task( 'bowerFilesFonts', function() {
    return gulp.src( plugins.mainBowerFiles('**/*.eot'))
        .pipe(gulp.dest('build/fonts'));
});

gulp.task( 'bowerFilesImages', function() {
    return gulp.src( plugins.mainBowerFiles('**/*.gif'))
        .pipe(gulp.dest('build/images'));
});

gulp.task( 'bowerFiles', gulp.series( 'bowerFilesJS', 'bowerFilesCSS', 'bowerFilesFonts', 'bowerFilesImages' ));

gulp.task('browser-sync', function() {
    browserSync.init({
        // lokale Wordpress Installation
        proxy: "localhost/devINDES",
        open: "false"
    });
});

gulp.task('js', function() {
    // var jsFiles = ['src/js/*'];
    // return gulp.src(plugins.mainBowerFiles().concat(jsFiles))
    return gulp.src( 'src/js/*.js' )
        .pipe(plugins.filter('**/*.js'))
        .pipe(plugins.concat('scripts.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest('build/js'));
});

gulp.task('php', function () {
    return gulp.src('src/**/*.php')
        .pipe(gulp.dest('build'));
});

gulp.task('styles', function() {
    gulp.src('src/scss/*.scss')
        .pipe(plugins.sass({outputStyle: 'expanded'}).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['>0.21% in at'],
            cascade: true
        }))
        .pipe(plugins.cleanCss({compatibility: '*'}))
        .pipe(gulp.dest('build'));
    return gulp.src('src/anleitung/scss/*.scss')
        .pipe(plugins.sass({outputStyle: 'expanded'}).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['>0.21% in at'],
            cascade: true
        }))
        .pipe(plugins.cleanCss({compatibility: '*'}))
        .pipe(gulp.dest('build/anleitung'))
});

gulp.task('md', function () {
    return gulp.src('src/anleitung/**/*.md')
        .pipe(plugins.markdown())
        .pipe(gulp.dest('./build/anleitung/'));
});

gulp.task('fileinclude', function() {
    return gulp.src(['./src/anleitung/*.html'])
        .pipe(fileinclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(gulp.dest('./build/anleitung/'));
});

gulp.task('dist', function() {
    gulp.src('build/**/*')
        .pipe(gulp.dest('dist'))
})

gulp.task('watch', gulp.parallel('styles', 'js', 'php', 'md', 'fileinclude'), function(){
    gulp.watch('./src/**/*.php', ['php']); 
    gulp.watch('./src/**/*.scss', ['styles']); 
    gulp.watch('./src/js/*.js', ['js']); 
    gulp.watch('./src/anleitung/**/*.md', ['md']);
    gulp.watch('./build/anleitung/**/*.html', ['fileinclude']); 
})

gulp.task('default', gulp.series('watch'));