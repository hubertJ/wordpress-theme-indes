const { task, src, dest, watch, series } = require( 'gulp' );

const gulpLoadPlugins = require( 'gulp-load-plugins' );

const plugins = gulpLoadPlugins();


function js() {
    return src( 'src/js/*.js' )
        .pipe( plugins.concat( 'scripts.js' ) )
        .pipe( dest( 'build/js' ) );
}

function php() {
    return src( 'src/**/*.php' )
        .pipe( dest( 'build/' ) );
}

function html() {
    return src( 'src/**/*.html' )
        .pipe( dest( 'build/' ) );
}

function themeCSS() {
    return src( 'src/scss/*.scss' )
        .pipe( plugins.sass({outputStyle: 'expanded'}).on( 'error', plugins.sass.logError ) )
        .pipe( plugins.autoprefixer({
            browsers: ['>0.21% in at'],
            cascade: true
        }) )
        .pipe( plugins.cleanCss( {compatibility: '*'} ) )
        .pipe( dest( 'build') );
}

function helpCSS() {
    return src( 'src/anleitung/scss/*.scss' )
        .pipe( plugins.sass({outputStyle: 'expanded'}).on( 'error', plugins.sass.logError ) )
        .pipe( plugins.autoprefixer({
            browsers: ['>0.21% in at'],
            cascade: true
        }))
        .pipe( plugins.cleanCss({compatibility: '*'}) )
        .pipe( dest( 'build/anleitung') );
}

function md() {
    return src( 'src/anleitung/**/*.md' )
        .pipe( plugins.markdown() )
        .pipe( dest( 'build/anleitung' ) );
}

function fileinclude() {
    return src( ['./src/anleitung/*.html'] )
        .pipe( plugins.fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe( dest( 'build/anleitung' ) );
}

function dist() {
    return src( 'build/**/*' )
        .pipe( dest( 'dist' ) );
}

task( js );
task( php );
task( html );
task( themeCSS );
task( helpCSS );
task( md );
task( fileinclude );
task( dist );


exports.build = series( 'js', 'themeCSS', 'helpCSS', 'md', 'fileinclude', 'dist' );

exports.default = function() {
    watch( 'src/js/*.js', series( 'js' ) );
    watch( 'src/**/*.php', series( 'php' ) );
    watch( 'src/**/*.html', series( 'html' ) );
    watch( 'src/scss/*.scss', series( 'themeCSS' ) );
    watch( 'src/anleitung/scss/*.scss', series( 'helpCSS' ) );
    watch( 'src/anleitung/**/*.md', series( 'md', 'fileinclude' ) );
}